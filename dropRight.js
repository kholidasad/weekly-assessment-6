function dropRight(arr, n) {
    if (n == 0) {
        return arr
    } else if (n == null) {
        arr.pop()
        return arr
    } else {
        for(let i = 0; i < n; i++) {
            arr.pop()
        }
        return arr
    }
}

console.log(dropRight([1, 2, 3]))
console.log(dropRight([1, 2, 3], 2))
console.log(dropRight([1, 2, 3], 5))
console.log(dropRight([1, 2, 3], 0))
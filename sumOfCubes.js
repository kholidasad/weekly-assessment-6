function sumOfCubes(arr) {
    if (!arr) {
        return 0
    }
    let hasil = 0
    for (let i = 0; i < arr.length; i++) {
        hasil += (arr[i] ** 3)
    }
    return hasil
}

console.log(sumOfCubes([1, 5, 9]));
console.log(sumOfCubes([3, 4, 5]));
console.log(sumOfCubes([2]));
console.log(sumOfCubes([]));